﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallManager : MonoBehaviour
{
    [Header("Ball settings")]
    public bool activated = false;
    public bool trown = false;
    public float speed = 10;
    public short movementsLeft = 3;
    public float maxSpeed = 50;
    public float minSpeed = 10;
    public Vector3 initialPos = new Vector3(37.08084f, -0.9825852f, 0.9176789f);
    public Vector3 actualPos = new Vector3(0,0,0);

    [Header("Track settings")]
    public Vector3 limits;

    public float speedMod = 1.5f;
    public float time = 2;

    bool IsMoving(Rigidbody body)
    {
        const float minSpeed = 0.005f;
        bool one = body.velocity.x > minSpeed || body.velocity.x < -minSpeed;
        bool two = body.velocity.y > minSpeed || body.velocity.y < -minSpeed;
        bool three = body.velocity.z > minSpeed || body.velocity.z < -minSpeed;

        return (one || two || three);
    }

    public void ResetFirstValues()
    {
        transform.localPosition = initialPos;
        trown = false;
        speed = minSpeed;
        activated = false;
        time = 2;
    }

    void Start()
    {
        if (SceneChanger.GetSceneName() != "Bowling") return;
        limits = GameObject.FindWithTag("Track").transform.localScale * 5;
    }

    void Update()
    {
        if (SceneChanger.GetSceneName() != "Bowling") return;

        GameObject gm = GameObject.FindWithTag("GameManager");

        if (gm.GetComponent<GameManager>().CheckGameEnded())
            return;

        //direction stuff
        float hor = Input.GetAxis("Horizontal");
        float ver = Input.GetAxis("Vertical");

        Vector3 direction = new Vector3(0, 0, hor);
        Vector3 finalDirection = transform.position + direction * speed * Time.deltaTime;

        //moving stuff
        Rigidbody body = GetComponent<Rigidbody>();

        //speed stuff
        float futureSpeed = speed + ver * Time.deltaTime * speedMod;

        actualPos = transform.position;

        //moving update
        if (activated)
        {
            if (time > 0)
            {
                body.velocity = new Vector3(-speed, 0, 0);
                time -= Time.deltaTime;
            }

            if (!IsMoving(body))
            {
                body.velocity = new Vector3(0, 0, 0);
                activated = false;
            }
        }
        else
        {
            if (finalDirection.z + transform.localScale.z <= limits.z && finalDirection.z - transform.localScale.z >= -limits.z)
                transform.position = finalDirection;

            //input update
            if (Input.GetKeyDown(KeyCode.Space) && !activated) 
            { 
                activated = true; 
                movementsLeft--;
                trown = true;
            }

            if (futureSpeed <= maxSpeed && futureSpeed >= minSpeed)
                speed = futureSpeed;

            if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.S)) speedMod += Time.deltaTime * speedMod;
            else speedMod = 1.5f;
        }
    }
}
