﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine.UI;
using UnityEngine;

public class BarManager : MonoBehaviour
{
    void Start()
    {
        if (SceneChanger.GetSceneName() != "Bowling") return;

        GameObject gm = GameObject.FindWithTag("GameManager");
        
        if (gm == null)
            return;

        const float minLenghtOfVisibleSpeed = 2.0f;
        GetComponent<Slider>().maxValue = GameObject.FindWithTag("GameManager").GetComponentInChildren<BallManager>().maxSpeed;
        GetComponent<Slider>().minValue = GameObject.FindWithTag("GameManager").GetComponentInChildren<BallManager>().minSpeed - minLenghtOfVisibleSpeed;
    }

    bool UpdatePosible(GameObject gm)
    {
        return gm != null && !gm.GetComponent<GameManager>().CheckGameEnded() && SceneChanger.GetSceneName() == "Bowling";
    }

    void Update()
    {
        GameObject gm = GameObject.FindWithTag("GameManager");

        if (!UpdatePosible(gm)) return;

        GetComponent<Slider>().value = gm.GetComponentInChildren<BallManager>().speed;
    }
}
