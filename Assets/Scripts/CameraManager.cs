﻿using UnityEngine;

public class CameraManager : MonoBehaviour
{
    private BallManager ball;
    private Vector3 initialPos;
    private float diferenceInPos = 0f;


    bool CheckPinesClose()
    {
        PinesManager componentPines = GameObject.FindWithTag("GameManager").GetComponentInChildren<PinesManager>();

        foreach (Pine.PineData pd in componentPines.pines)
        {
            if (pd.pos.x >= ball.actualPos.x)
                return true;
        }

        return false;
    }

    void Start()
    {
        initialPos = transform.position;

    }

    void LateUpdate()
    {
        if (SceneChanger.GetSceneName() != "Bowling") return;

        ball = GameObject.FindWithTag("GameManager").GetComponentInChildren<BallManager>();

        if (diferenceInPos == 0)
            diferenceInPos = transform.position.x - ball.actualPos.x;

        if (!CheckPinesClose())
            transform.position = new Vector3(ball.actualPos.x + diferenceInPos, transform.position.y, transform.position.z);
    }
}
