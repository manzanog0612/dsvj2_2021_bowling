﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class EndSceneManager : MonoBehaviour
{
    [SerializeField] Text textFinal;
    [SerializeField] Text textData;
    [SerializeField] Color colorFinalText;
    [SerializeField] GameObject speedBar;
    //[SerializeField] Image image;*/

    void SetFinalSceneOverlay()
    {
        if (SceneChanger.win)
            textFinal.text = "You Won!";
        else
            textFinal.text = "You Lost!";

        textFinal.fontSize = 43;
        textFinal.alignment = TextAnchor.MiddleCenter;
        textFinal.color = colorFinalText;
        textData.color = colorFinalText;
    }

    void Start()
    {
        SetFinalSceneOverlay();
        if (SceneChanger.GetSceneName() == "Bowling") 
        {
            textData.text = "Ball speed " + " \nTries left: " + SceneChanger.finalData.tries + "\nPines left: " + SceneChanger.finalData.pinesAlive + "\nScore: " + SceneChanger.finalData.score;

            speedBar.GetComponent<Slider>().maxValue = SceneChanger.finalData.maxSpeed;
            speedBar.GetComponent<Slider>().minValue = SceneChanger.finalData.minSpeed;
            speedBar.GetComponent<Slider>().value = SceneChanger.finalData.ballSpeed;
        }
        else
        {
            textData.text = "Pines left: " + SceneChanger.finalData.pinesAlive + "\nScore: " + SceneChanger.finalData.score;
        }

            
    }

    void Update()
    {
    }
}
