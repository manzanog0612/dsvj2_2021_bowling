﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplotionPinesManager : MonoBehaviour
{
    GameObject prefab;

    List<ExplotionPine> ePines;

    int amountPines;
    int amountCircunferences = 1;

    float radius;
    float parentPineHeight;

    float force;

    void SetAmountCircunferencesOfInstancing()
    {
        const float maxAngle = 360.0f;
        const float minAngle = 15.0f;
        float angle = 0;

        for (int i = 0; i < amountPines; i++)
        {
            angle += minAngle;

            if (angle == maxAngle)
            {
                amountCircunferences++;
                angle = 0;
            }
        }
    }

    int GetDistanceAxisBetweenPines(float radius)
    {
        if (amountPines == 1)
            return 0;
        else
            return 360 / amountPines / amountCircunferences;
    }

    public void Instantiation(GameObject parent, float posY)
    {
        //posY is the diference between the hidden and the original position in Y axis of the parent GameObject;

        SetAmountCircunferencesOfInstancing();

        int axis = 0;
        int nextSpawnAngle = GetDistanceAxisBetweenPines(radius);
        float initialRadius = radius;

        const float distanceCenterEPineToParentPine = 1.5f;

        for (int i = 0; i < amountCircunferences; i++)
        {
            while (axis <= 360.0f)
            {
                if (nextSpawnAngle == axis)
                {
                    Vector3 pos = new Vector3(radius * Mathf.Cos(axis), parentPineHeight + distanceCenterEPineToParentPine + posY, radius * Mathf.Sin(axis));

                    GameObject eP = Instantiate(prefab, Vector3.zero, Quaternion.identity).gameObject;
                    eP.transform.parent = parent.transform;
                    eP.transform.position = parent.transform.position + pos;
                    eP.GetComponent<ExplotionPine>().initialForce = new Vector3(Mathf.Cos(axis) * force, force, Mathf.Sin(axis) * force);
                    ePines.Add(eP.GetComponent<ExplotionPine>());

                    nextSpawnAngle += GetDistanceAxisBetweenPines(radius); 
                }
                axis ++;

            }

            radius += initialRadius;
            axis = 0;
        }
    }

    void Start()
    {
        amountPines = GameObject.FindWithTag("PinesManager").GetComponent<PinesManager>().amountExplotionPines;
        force = GameObject.FindWithTag("PinesManager").GetComponent<PinesManager>().ePinesForce;

        ePines = new List<ExplotionPine>();

        prefab = GameObject.FindWithTag("PinesManager").GetComponent<PinesManager>().ePinePrefab;

        radius = prefab.transform.localScale.x;
        parentPineHeight = prefab.transform.localScale.y;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
