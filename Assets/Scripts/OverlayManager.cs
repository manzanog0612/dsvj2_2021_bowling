﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class OverlayManager : MonoBehaviour
{
    [SerializeField] Text text;

    void Start()
    {
        text = GameObject.FindWithTag("UI_in_game").GetComponent<Text>();
    }

    void Update()
    {
        GameObject gm = GameObject.FindWithTag("GameManager");

        int pinesAlive = gm.GetComponent<GameManager>().pinesAlive;
        int pinesCount = gm.GetComponentInChildren<PinesManager>().pines.Count;
        int score;

        if (SceneChanger.GetSceneName() == "Bowling")
        {
            score = (pinesCount - pinesAlive) * 10;
            short tries = gm.GetComponentInChildren<BallManager>().movementsLeft;

            text.text = "Ball speed " + " \nTries left: " + tries + "\nPines left: " + pinesAlive + "\nScore: " + score;
        }
        else
        {
            const short totalPines = 10;

            score = (totalPines - pinesAlive) * 10;
            text.text = "Pines left: " + pinesAlive + "\nScore: " + score;
        }
    }
        
}
