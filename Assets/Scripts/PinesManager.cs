﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PinesManager : MonoBehaviour
{
    public GameObject pinePrefab;
    public GameObject ePinePrefab;

    public int amountExplotionPines = 2;
    public float ePinesForce;

    public List<Pine.PineData> pines = new List<Pine.PineData>();

    private Vector2 min;
    private Vector2 max;

    public Camera cam;

    public bool CheckAnyMoving()
    {
        foreach (Pine.PineData p in pines)
        {
            if (p.moving)
                return true;
        }

        return false;
    }

    void Start()
    {
        cam = Camera.main;

        Transform t = GameObject.FindWithTag("TrackPlane").transform;
        float pinesGap = 1.5f;
        Vector3 pos = new Vector3(0, 0, 0);

        Pine.totalPines = 0;

        for (int i = 0; i < 5; i++)
        {
            pos.z = i * -pinePrefab.transform.localScale.x + pinesGap;

            for (int j = 0; j < i; j++)
            {
                pos.z = -pinePrefab.transform.localScale.x * i + j * pinePrefab.transform.localScale.x * 2 + 1;
                pos.x = i * -pinePrefab.transform.localScale.x - 16;
                pos.y = 3;

                GameObject go = Instantiate(pinePrefab, new Vector3(0, 0, 0), Quaternion.identity).gameObject;
                go.transform.parent = GameObject.FindWithTag("PinesManager").transform;
                go.transform.position = pos;

                go.GetComponent<Pine>().InitData();
                pines.Add(go.GetComponent<Pine>().GetData());
            }
        }

        min = new Vector2(t.position.x - t.localScale.x * 5 / 2, t.position.z - t.localScale.z * 5 / 2);
        max = new Vector2(t.position.x + t.localScale.x * 5 / 2, t.position.z + t.localScale.z * 5 / 2);
    }


    void Update()
    {
        GameObject gm = GameObject.FindWithTag("GameManager");
        if (gm.GetComponent<GameManager>().CheckGameEnded() || SceneChanger.GetSceneName() != "Bowling")
            return;

        Vector2 pos = new Vector2(Random.Range(min.x, max.x), Random.Range(min.y, max.y));

        if (Input.GetKeyDown(KeyCode.P))
        {
            GameObject go = Instantiate(pinePrefab, new Vector3(pos.x, 3, pos.y), Quaternion.identity).gameObject;
            go.transform.parent = GameObject.FindWithTag("PinesManager").transform;

            go.GetComponent<Pine>().InitData();
            pines.Add(go.GetComponent<Pine>().GetData());
        }
    }
}
