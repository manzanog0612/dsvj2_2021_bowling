﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class SceneChanger : MonoBehaviour
{
    private static SceneChanger instance;

    public static bool goToMenu = false;
    public static bool goToCredits = false;
    public static bool goToBowling = false;
    public static bool goToGunMode = false;

    public static bool inEndScene = false;

    public static bool win = false;
    public static bool lose = false;

    public class FinalUserData
    {
        public short tries;
        public int pinesAlive;
        public int score;
        public float ballSpeed;
        public float maxSpeed;
        public float minSpeed;
    }

    public static FinalUserData finalData;

    public static SceneChanger Get()
    {
        return instance;
    }

    public static string GetSceneName()
    {
        return SceneManager.GetActiveScene().name;
    }

    public bool GetWin()
    {
        return win;
    }

    public bool GetLose()
    {
        return lose;
    }

    private void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
            Debug.Log(name + " has been destroyed, there's already one Scene Manager created.");
            return;
        }
        instance = this;
        Debug.Log(name + " has been created and is now the Scene Manager.");
        DontDestroyOnLoad(gameObject);
    }

    void ResetEndConditions()
    {
        if (win)
            win = false;

        if (lose)
            lose = false;
    }

    private void Update()
    {
        if((win || lose) && !inEndScene)
        {
            SceneManager.LoadScene("EndScene");
            inEndScene = true;
        }

        if (goToMenu)
        {
            ResetEndConditions();
            SceneManager.LoadScene("Menu");
            goToMenu = false;
            if (inEndScene) inEndScene = false;
        }

        if (goToGunMode)
        {
            ResetEndConditions();
            SceneManager.LoadScene("GunMode");
            goToGunMode = false;
        }

        if (goToBowling)
        {
            ResetEndConditions();
            SceneManager.LoadScene("Bowling");
            goToBowling = false;
        }

        if (goToCredits)
        {
            ResetEndConditions();
            SceneManager.LoadScene("Credits");
            goToCredits = false;
        }

    }
}
