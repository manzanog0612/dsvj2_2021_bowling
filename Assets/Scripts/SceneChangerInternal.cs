﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class SceneChangerInternal : MonoBehaviour
{
    public void GoToMenu()
    {
        SceneChanger.goToMenu = true;
    }

    public void GoToNormalBowling()
    {
        SceneChanger.goToBowling = true;
    }

    public void GoToGunMode()
    {
        SceneChanger.goToGunMode = true;
    }

    public void GoToCredits()
    {
        SceneChanger.goToCredits = true;
    }
}
