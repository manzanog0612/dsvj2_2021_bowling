﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotsManager : MonoBehaviour
{
    //Camera cam;

    bool CheckPineHit(GameObject go)
    {
        return go.tag == "Pine";
    }

    bool UpdatePosible(GameObject gm)
    {
        return gm != null && !gm.GetComponent<GameManager>().CheckGameEnded() && SceneChanger.GetSceneName() != "Bowling";
    }

    void Start()
    {
        //cam = Camera.main;
    }

    void Update()
    {
        GameObject gm = GameObject.FindWithTag("GameManager");
        if (!UpdatePosible(gm)) return;

        float x = Input.GetAxis("Mouse X");
        float y = Input.GetAxis("Mouse Y");
       
        if (Input.GetMouseButtonDown(0))
        {
            SetShot();
        }
    }

    public void SetShot()
    {
        Camera cam = GameObject.FindWithTag("PinesManager").GetComponent<PinesManager>().cam;

        Vector3 mousePos = Input.mousePosition;
        Ray ray = cam.ScreenPointToRay(mousePos);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, 200))
        {
            if (!CheckPineHit(hit.transform.gameObject))
                GameObject.FindWithTag("GameManager").GetComponent<GameManager>().shootedOtherThing = true;
        }
    }
}
